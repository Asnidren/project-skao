module.exports = function(grunt) {
  grunt.warn = grunt.log;
	grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    date: grunt.template.today("yyyy-mm-dd"),
    banner: '/*! <%= pkg.name %> <%= date %> */\n',



    yaml_validator: {
      options: {
        // Task-specific options go here. 
      },
      sample: { 
        src: ['sample/config/**.yml','sample/config/**/*.yml']
      },
      config: {
        src: ['etc/config/*.yml','etc/config/**/*.yml']
      },
      src: {
        src: ['src/data/**/*.yml','src/module/**/*.properties']
      },
    },
    
    
    allsrcjs: '<%= src.js %>/**/*.js',
    allsrcimg: '**/*.{png,jpg,gif}',


    mkdir: {
      etc: {
        options: {
          create: [
            'etc',
            'etc/complib',
            'etc/distlib',
          ]
        }
      },
      src: {
        options: {
          create: [
            '<%= src.img %>/contents',
            '<%= src.img %>/css',
            '<%= src.js %>',
            '<%= src.scss %>',
            '<%= src.hbsdata %>',
            '<%= src.distpages %>/static',
            '<%= src.distpages %>/api',
            '<%= src.distpages %>/formatters',
            '<%= src.hbs %>/formatters',
            '<%= src.hbs %>/nested',
            '<%= src.hbs %>/templates',
            '<%= src.java %>',
            '<%= src.module %>/system/modules/<%= module.module.name %>',
          ]
        }
      },
      build: {
        options: {
          create: [
            '<%= build.css %>',
            '<%= build.js %>',
            '<%= build.img %>',
            '<%= build.classes %>',
            '<%= build.jar %>',
            '<%= dist.javadoc %>',
            '<%= dist.static %>',
            '<%= dist.api %>',
            '<%= dist.module %>',
            '<%= zips.static %>',    // zip des templates statiques
            '<%= zips.api %>',       // zip des pages api
            '<%= zips.module %>'    // zip du module
          ]
        }
      },
      buildcss: {
        options: {
          create: [
            '<%= build.css %>'
          ]
        }
      },
      buildjs: {
        options: {
          create: [
            '<%= build.js %>'
          ]
        }
      },
      buildhtml: {
        options: {
          create: [
            '<%= dist.static %>',
            '<%= dist.api %>',
            '<%= dist.module %>'
          ]
        }
      },
      buildjava: {
        options: {
          create: [
            '<%= build.classes %>',
            '<%= build.jar %>',
            '<%= dist.javadoc %>'
          ]
        }
      },
      module: {
        options: {
          create: [
            '<%= src.module %>/system/modules/<%= module.module.name %>'
          ]
        }
      }
    },

    clean: {
      build: [
        '<%= build.css %>/**',
        '<%= build.js %>/**',
        '<%= build.img %>/**',
        '<%= build.classes %>/**',
        '<%= build.jar %>/**',
        '<%= dist.javadoc %>/**',
        '<%= dist.static %>/**',
        '<%= dist.api %>/**',
        '<%= dist.module %>/**'
      ],
      buildcss: [
        '<%= build.css %>/**'
      ],
      buildjs: [
        '<%= build.js %>/**'
      ],
      buildhtml: [
        '<%= dist.static %>/**',
        '<%= dist.api %>/**',
        '<%= dist.module %>/**'
      ],
      buildjava: [
        '<%= build.classes %>/**',
        '<%= build.jar %>/**',
        '<%= dist.javadoc %>/**'
      ],
    },

    copy: {
      js: {
        files: [
          {expand: true, cwd: '<%= src.js %>/', src: ['**'], dest: '<%= build.js %>/src/'}
        ]
      },
      module: {
        files: [
          { expand: true,  cwd: '<%= src.module %>/', src: ['**','!**/*.properties','!**/__properties','!**/__properties/**'], dest: '<%= dist.module %>/' },
          { expand: true,  cwd: '<%= build.css %>/', src: ['**'],  dest: '<%= dist.module %>/system/modules/<%= module.module.name %>/resources/css/' },
          { expand: true,  cwd: '<%= build.js %>/', src: ['**'],  dest: '<%= dist.module %>/system/modules/<%= module.module.name %>/resources/js/' },
          { expand: true,  cwd: '<%= build.jar %>/', src: ['**'],  dest: '<%= dist.module %>/system/modules/<%= module.module.name %>/lib/' },
          { expand: true,  cwd: 'etc/distlib/', src: ['**'],  dest: '<%= dist.module %>/system/modules/<%= module.module.name %>/lib/' },
          { expand: true,  cwd: '<%= build.img %>/', src: ['**'],  dest: '<%= dist.module %>/system/modules/<%= module.module.name %>/resources/images/' },
          //{ expand: true,  cwd: '<%= dist.formatters %>/', src: ['**'],  dest: '<%= dist.module %>/system/modules/<%= module.module.name %>/formatters-hbs/' }
        ]
      }
    },

    sass: {
			options: {
				sourceMap: true
			},
			build: {
				files: {
					'<%= build.css %>/styles.min.css': '<%= src.scss %>/styles.scss'
				}
			}
		},

    

    jshint: {
      src: ['<%= allsrcjs %>']
    },


    
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      build: {
        files: {
          '<%= build.js %>/scripts.min.js': [
            '<%= src.js %>/script.js'
          ]
        }
      }
    },

    

    imagemin: { 
      dynamic: {                         // Another target
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'src/img/',                   // Src matches are relative to this path
          src: ['<%= allsrcimg %>'],   // Actual patterns to match
          dest: 'build/img/'                  // Destination path prefix
        }]
      }
    },
    

    

    assemble: {
      options:{ 
        flatten: true,
        data: '<%= src.hbsdata %>/**/*.yml',
        helpers: 'etc/assemble-helpers/*.js',
        partials: '<%= src.hbs %>/**/*.hbs',
      },
      static: {
        options: {
          layout: 'etc/assemble-layouts/static.hbs'/*,
          data: '<%= src.hbsdata %>/static/*.yml'*/
        },
        files: {
          '<%= dist.static %>/': ['<%= src.distpages %>/static/*.hbs','<%= src.distpages %>/static/**/*.hbs']
        }
      },
      api: {
        options: {
          layout: 'etc/assemble-layouts/api.hbs'/*,
          data: '<%= src.hbsdata %>/api/*.yml'*/
        },
        files: {
          '<%= dist.api %>/': ['<%= src.distpages %>/api/*.hbs','<%= src.distpages %>/api/**/*.hbs']
        }
      },
      formatters: {
        options: {
          layout: 'etc/assemble-layouts/formatter.hbs',/*
          data: '<%= src.hbsdata %>/formatters/*.yml',*/
          ext: '.jsp'
        },
        files: {
          '<%= dist.formatters %>/': ['<%= src.distpages %>/formatters/*.hbs','<%= src.distpages %>/formatters/**/*.hbs']/*,
          '<%= dist.module %>/system/modules/<%= module.module.name %>/formatters/': ['<%= src.hbs %>/bodies-formatters/*.hbs']*/
        }
      }
    },

    compress: {
      static: {
        options: {
          archive: '<%= zips.static %>/archive-<%= date %>.zip'
        },
        files: [
          {expand: true, cwd: '<%= dist.static %>/', src: ['**']}
        ]
      },
      api: {
        options: {
          archive: '<%= zips.api %>/archive-<%= date %>.zip'
        },
        files: [
          {expand: true, cwd: '<%= dist.api %>/', src: ['**']}
        ]
      },
      module: {
        options: {
          archive: '<%= zips.module %>/<%= module.module.name %>-<%= module.module.version %>.zip'
        },
        files: [
          {expand: true, cwd: '<%= dist.module %>/', src: ['**']}
        ]
      }
    },

    run_java: {
      options: {
        // Task-specific options go here.
      },
      java: {
        // Target-specific details go here. 
        execOptions:{
          cwd: "./"
        }, 
        command: "java",  
        javaOptions: { //javac Options 
          "version": ""
        }
      },
      javac: {
        // Target-specific details go here. 
        execOptions:{
          cwd: "./"
        }, 
        command: "javac",  
        javaOptions: { //javac Options 
          "classpath": ["etc/complib/","etc/distlib/"],
          "d": "<%= build.classes %>/",
          "target": "<%= cfg.java.jdk %>",
          "source": "<%= cfg.java.javasource %>",
          //"sourcepath": ['src/java/'] // nb: a reporter dans sourceFiles ci-dessous
        },
        sourceFiles: ['<%= src.java %>/*.java']
      },
      jar: {
        // Target-specific details go here. 
        execOptions:{
          cwd: "./"
        }, 
        command: "jar",  
        jarOptions : "cmf", //{ctxui}[vfm0Me]
        manifestName: "etc/config/MANIFEST.MF",
        jarName: "<%= build.jar %>/<%= cfg.project.package %>.jar",
        dir: "./",
        files: "<%= build.classes %>/"
      },
      javadoc: {
        // Target-specific details go here. 
        execOptions:{
          cwd: "./"
        }, 
        command: "javadoc",  
        javadocOptions: {
          "d": "<%= dist.javadoc %>/",
        },
        sourceFiles: ['<%= src.java %>/*.java']
      },
    },

    manifestjava: {
      options:{
        project: 'etc/config/project.yml'
      },
      jar: {

      }
    },


    manifest: {
      options:{
        project: 'etc/config/project.yml',
        moduleexport: 'etc/config/moduleexport.yml',
        types: 'etc/config/types/'
      },
      opencms: {

      }
    },

    inittypes: {
      options:{
        project: 'etc/config/project.yml',
        moduleexport: 'etc/config/moduleexport.yml',
        types: 'etc/config/types/'
      },
      opencms: {

      }
    },

    updatemoduleconfig: {
      options:{
        project: 'etc/config/project.yml',
        moduleexport: 'etc/config/moduleexport.yml',
        moduleconfig: 'etc/config/moduleconfig.yml',
        types: 'etc/config/types/'
      },
      opencms: {

      }
    },

    cfg: grunt.file.readYAML('etc/config/project.yml'),
    src: grunt.file.readYAML('etc/config/project.yml').folders.src,
    build: grunt.file.readYAML('etc/config/project.yml').folders.build,
    dist: grunt.file.readYAML('etc/config/project.yml').folders.dist,
    zips: grunt.file.readYAML('etc/config/project.yml').folders.zips,
    module: grunt.file.readYAML('etc/config/moduleexport.yml'),

  });


  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-assemble');
  grunt.loadNpmTasks('grunt-run-java');
  grunt.loadNpmTasks('grunt-yaml-validator');
  grunt.loadTasks('tasks');



  grunt.registerTask('init', ['mkdir:etc','mkdir:src','mkdir:build','inittypes','updatemoduleconfig']);

  // update css
  grunt.registerTask('css', ['clean:buildcss','mkdir:buildcss','sass']);

  // update js
  grunt.registerTask('js', ['clean:buildjs','mkdir:buildjs','jshint','copy:js','uglify']);

  // update html
  grunt.registerTask('html', ['clean:buildhtml','mkdir:buildhtml','assemble:static','assemble:api','assemble:formatters']);

  // build dist tpl stq & api
  grunt.registerTask('static', ['css','js','html']);

  // build java
  grunt.registerTask('java', ['clean:buildjava','mkdir:buildjava','manifestjava','run_java:java','run_java:javac','run_java:jar'/*,'javadoc'*/]);

  // build dist module
  grunt.registerTask('module', ['updatemoduleconfig','copy:module','manifest','compress:module']);

  grunt.registerTask('default', ['static','java','module']);

  // question : ou mettre le 4e num de version?


  // TODO :
  /*
  gestion des parametres de javac + fork run_java pour javadoc
  tache de check entre config initiale et contenu du dist
  construction d'un template d'api sympa

  ajout des properties sur inittypes de formatters
  */


  grunt.registerTask('log','log',function(){
    //console.log(grunt.file.readJSON('package.json'));
    //console.log(grunt.file.readJSON('config.json'));
    //var cfg = grunt.file.readJSON('config.json');
    //console.log(cfg.folders);
    grunt.log.error('truc');
    grunt.warn.error('truc');
  });
}