/*
 * updatemoduleconfig
 * 
 *
 * Copyright (c) 2015 SandrineProusteau
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks


  var getTypesFiles = function(options){
    var typesList = [];
    var allTypeFiles = grunt.file.expand({cwd:options.types,filter: 'isFile'},['**.yml']);
    console.log('Opencms resources types config files : ' + allTypeFiles);
    if(allTypeFiles.length>0){
      for(var i = 0; i < allTypeFiles.length; i++){
        var path = allTypeFiles[i];
        var typefile = grunt.file.readYAML(options.types + path);
        typesList.push(typefile);
      }
    }
    //console.log('Opencms resources types : ');
    //console.log(typesList);
    return typesList;
  };

  var doInitType = function(type){

    try{
      if(type.moduleconfig!==undefined && type.moduleconfig!==false && type.moduleconfig!==null){
        var result = '<ResourceType>\n'+
      		'  <TypeName><![CDATA['+type.name+']]></TypeName>\n'+
      		'  <Folder>\n'+
        	'    <'+type.moduleconfig.folder.mode+'><![CDATA['+type.moduleconfig.folder.value+']]></Name>\n'+
      		'  </Folder>\n'+
      		'  <NamePattern><![CDATA['+type.moduleconfig.namepattern+']]></NamePattern>\n'+
      		'  <Order><![CDATA['+type.moduleconfig.order+']]></Order>\n'+
      		'  <DetailPagesDisabled>'+type.moduleconfig.detailpagesdisabled+'</DetailPagesDisabled>\n'+
    			'</ResourceType>\n';
    		return result;
      }
      return '';
    }catch(err){
      console.error('doInitType() : ' + err);
      return '';
    }

  };

  var doInitProperties = function(cfg){

    try{
    	var r = '';
      if(cfg.pageproperties!==undefined && cfg.pageproperties!==false && cfg.pageproperties!==null){
        if(cfg.pageproperties.length>0){
        	for(var p = 0; p < cfg.pageproperties.length; p++){
        		var prop = cfg.pageproperties[p];
        		var result = '<Property>\n'+
				      '  <PropertyName><![CDATA['+prop.propertyname+']]></PropertyName>\n'+
				      '  <DisplayName><![CDATA['+prop.displayname+']]></DisplayName>\n'+
				      '  <Description><![CDATA['+prop.description+']]></Description>\n'+
				      '  <Widget><![CDATA['+prop.widget+']]></Widget>\n'+
				      '  <Default><![CDATA['+prop.default+']]></Default>\n'+
				      '  <WidgetConfig><![CDATA['+prop.widgetconfig+']]></WidgetConfig>\n'+
				    	'</Property>\n';
				    r += result;
        	}
        }
      }
      return r;
    }catch(err){
      console.error('doInitType() : ' + err);
      return '';
    }

  };

  grunt.registerMultiTask('updatemoduleconfig', 'Create or update module .config file', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
     project: 'etc/config/project.yml',
     moduleexport: 'etc/config/moduleexport.yml',
     moduleconfig: 'etc/config/moduleconfig.yml',
     types: 'etc/config/types/'

    });
    //grunt.log.writeln('Final options = ' + JSON.stringify(options, null, 4));
    //grunt.log.writeln('arguments = ' + JSON.stringify(arguments, null, 4));

    var project = grunt.file.readYAML(options.project);
    var moduleexport = grunt.file.readYAML(options.moduleexport);
    var typesList = getTypesFiles(options);
    var moduleconfig = grunt.file.readYAML(options.moduleconfig);

    //grunt.log.writeln(JSON.stringify(moduleexport.module, null, 4));

    var today = new Date();
    var dateString = grunt.template.date(today, "ddd, dd mmm yyyy HH:mm:ss") + ' CEST';

    var resourcetypes = '';
    if(typesList!==undefined && typesList.length>0){
      for(var t = 0; t < typesList.length; t++){
        var type = typesList[t];
        //console.log(type);
        resourcetypes += doInitType(type);
      }
    }

    var properties = doInitProperties(moduleconfig);

    var contents = '<?xml version="1.0" encoding="UTF-8"?>\n';
    contents += 
      '<ModuleConfigurations xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="opencms://system/modules/org.opencms.ade.config/schemas/module_config.xsd">\n'+
  		' <ModuleConfiguration language="en">\n'+
      	resourcetypes+
      	properties+
      ' </ModuleConfiguration>\n'+
			'</ModuleConfigurations>\n';

    grunt.file.write( project.folders.src.module+'/system/modules/' + moduleexport.module.name+'/.config', contents);

    contents = project.modulepropertiesdefinition.resourcetype;
    if(project.modulepropertiesdefinition.separator==='='){
    	contents += '=';
    }else{
    	contents += ': ';
    }
    contents += 'module_config';
    grunt.file.write( project.folders.src.module+'/system/modules/' + moduleexport.module.name+'/__properties/.config.properties', contents);

  });

};
