/*
 * manifestjava
 * 
 *
 * Copyright (c) 2015 SandrineProusteau
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('manifestjava', 'Create OpenCms java manifest', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
     project: 'etc/config/project.yml'

    });
    //grunt.log.writeln('Final options = ' + JSON.stringify(options, null, 4));
    //grunt.log.writeln('arguments = ' + JSON.stringify(arguments, null, 4));

    var project = grunt.file.readYAML(options.project);

    var today = new Date();
    var dateString = grunt.template.date(today, "ddd, dd mmm yyyy HH:mm:ss") + ' CEST';

       
    var contents = 'Manifest-Version: ' + project.java.version + '\n'+
			'Created-By: ' + project.java.creator + '\n' +
			'Build-Jdk: ' + project.java.jdk + '\n';

    grunt.file.write( project.folders.build.classes+'/MANIFEST.MF', contents);
    console.log(project.folders.build.classes+'/MANIFEST.MF updated.');
    
  });

}