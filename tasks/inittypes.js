/*
 * initypes
 * initialise les fichiers xsd et jsp (avec .properties) d'apres la config
 *
 * Copyright (c) 2015 SandrineProusteau
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks


  var getTypesFiles = function(options){
    var typesList = [];
    var allTypeFiles = grunt.file.expand({cwd:options.types,filter: 'isFile'},['**.yml']);
    console.log('Opencms resources types config files : ' + allTypeFiles);
    if(allTypeFiles.length>0){
      for(var i = 0; i < allTypeFiles.length; i++){
        var path = allTypeFiles[i];
        var typefile = grunt.file.readYAML(options.types + path);
        typesList.push(typefile);
      }
    }
    //console.log('Opencms resources types : ');
    //console.log(typesList);
    return typesList;
  };

  var doInitType = function(type,cfg,module){

    // XSD
    try{
      if(type.resourcetype!==false){
        var params = type.resourcetype.params;
        //console.log(params);
        if(params!==undefined && params!==false && params!==null && params.length>0){
          for(var p = 0; p< params.length; p++){
            var param = params[p];
            if(param.name==='schema'){
              var xsd = param.value.trim();
              var path = cfg.folders.src.module + '/system/modules/' + module.module.name + '/schemas/' + xsd;
              //console.log(path);
              if(!grunt.file.exists(path)){
                //console.log(path + ' doesnt exists.');
                grunt.file.write(path,'');
                console.log('XSD ' + path + ' created.');
              }
            }
          }
        }
      }
    }catch(err){
      console.error('doInitType() - XSD : ' + err);
    }
    
    // formatters
    try{
      var formatters = type.formatters;
      //console.log(formatters);
      if(formatters!==undefined && formatters!==false && formatters!==null && formatters.length>0){
        for(var p = 0; p< formatters.length; p++){
          var formatter = formatters[p];
          var partpath = formatter.path.trim();
          var slashIndex = partpath.lastIndexOf('/');

          // formatter hbs
          var hbspath;
          if(slashIndex===-1){
            hbspath = cfg.folders.src.hbs + '/formatters/formatter__' + partpath.replace('.jsp','.hbs');
          }else{
            hbspath = cfg.folders.src.hbs + '/formatters/' + partpath.substr(0, slashIndex) + '/formatter__' + partpath.substr(slashIndex+1, partpath.length).replace('.jsp','.hbs');
          }
          //console.log(path);
          if(!grunt.file.exists(hbspath)){
            grunt.file.write(hbspath,'');
            console.log('Hbs formatter ' + hbspath + ' created.');
          }

          // instance bodies-api
          var bodypath = cfg.folders.src.distpages + '/api/' + partpath.replace('.jsp','.hbs');
          //console.log(path);
          if(!grunt.file.exists(bodypath)){
            var contents = '---\n'+
              'api:\n'+
              '  object: '+type.api.object+'\n'+
              '  type: '+type.name+'\n'+
              '  description: '+type.api.description+'\n'+
              'partialname: formatter__'+type.name+'\n'+
              'samples: \n'+
              '  - title: Example 1\n' +
              '    component: <%= '+type.name+'__simple %>\n'+
              '---\n';
            grunt.file.write(bodypath,contents);
            console.log('Hbs body api ' + bodypath + ' created.');
          }

          // data
          var datapath = cfg.folders.src.hbsdata + '/' + type.api.object + '/' + type.name + '__simple.yml';
          //console.log(path);
          if(!grunt.file.exists(datapath)){
            grunt.file.write(datapath,'');
            console.log('Hbs data ' + datapath + ' created.');
          }

          // instance bodies-formatters
          var bodyfpath = cfg.folders.src.distpages + '/formatters/' + partpath.replace('.jsp','.hbs');
          //console.log(path);
          if(!grunt.file.exists(bodyfpath)){
            var contents = '---\n'+
              'header: >\n'+
              '  header\n'+
              'footer: >\n'+
              '  footer\n' +
              'partialname: formatter__' + type.name + '\n' +
              'component: <%= ' + type.name + '__to_jsp %>\n' +
              '---\n';
            grunt.file.write(bodyfpath,contents);
            console.log('Hbs body formatter ' + bodyfpath + ' created.');
          }

          // data formatter
          var datafpath = cfg.folders.src.hbsdata + '/formatters/' + type.name + '__to_jsp.yml';
          //console.log(datafpath);
          if(!grunt.file.exists(datafpath)){
            grunt.file.write(datafpath,'');
            console.log('Hbs data ' + datafpath + ' created.');
          }

          // formatter direct dans module
          var path = cfg.folders.src.module + '/system/modules/' + module.module.name + '/formatters/' + partpath;
          //console.log(path);
          if(!grunt.file.exists(path)){
            grunt.file.write(path,'');
            console.log('Module formatter ' + path + ' created.');
          }

          // .properties du formatter du module
          var slashIndex = partpath.lastIndexOf('/');
          var proppath;
          if(slashIndex===-1){
            proppath = cfg.folders.src.module + '/system/modules/' + module.module.name + '/formatters/__properties/' + partpath + '.properties';
          }else{
            proppath = cfg.folders.src.module + '/system/modules/' + module.module.name + '/formatters/' + partpath.substr(0, slashIndex) + '/__properties' + partpath.substr(slashIndex, partpath.length) + '.properties';
          }
          if(proppath!==undefined && !grunt.file.exists(proppath)){
            //console.log(path + ' doesnt exists.');
            var contents = '';
            contents += cfg.modulepropertiesdefinition.resourcetype + (cfg.modulepropertiesdefinition.separator==='=' ? '=' : ':') + 'jsp';
            grunt.file.write(proppath,contents);
            console.log('Module formatter properties ' + proppath + ' created.');
          }
          
        }
      }
    }catch(err){
      console.error('doInitType() - Formatters : ' + err);
    }

  };

  grunt.registerMultiTask('inittypes', 'Create OpenCms types files', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
     project: 'etc/config/project.yml',
     moduleexport: 'etc/config/moduleexport.yml',
     types: 'etc/config/types/'

    });
    //grunt.log.writeln('Final options = ' + JSON.stringify(options, null, 4));
    //grunt.log.writeln('arguments = ' + JSON.stringify(arguments, null, 4));

    var project = grunt.file.readYAML(options.project);
    var moduleexport = grunt.file.readYAML(options.moduleexport);
    var typesList = getTypesFiles(options);

    //grunt.log.writeln(JSON.stringify(moduleexport.module, null, 4));

    var today = new Date();
    var dateString = grunt.template.date(today, "ddd, dd mmm yyyy HH:mm:ss") + ' CEST';

    if(typesList!==undefined && typesList.length>0){
      for(var t = 0; t < typesList.length; t++){
        var type = typesList[t];
        //console.log(type);
        doInitType(type,project,moduleexport);
      }
    }

    //grunt.file.write( project.folders.dist.module+'/manifest.xml', contents);

    // parcourir les types dans etc/config/types
    // recuperer resourcetype.params de name schema + xsdtag, creer dans src/module
    // recuperer les formatters path, creer soit dans src/module, soit dans src/hbs
    
  });

};
