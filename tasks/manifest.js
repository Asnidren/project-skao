/*
 * manifest
 * 
 *
 * Copyright (c) 2015 SandrineProusteau
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks
  
  var getDependencies = function(cfg){
    var dependencies = '   <dependencies/>\n';
    if(cfg.module.dependencies!==undefined && cfg.module.dependencies!==false && cfg.module.dependencies!==null && cfg.module.dependencies.length>0){
      dependencies = '   <dependencies>\n';
      for(var iDependency = 0; iDependency < cfg.module.dependencies.length; iDependency++){
        var dependency = cfg.module.dependencies[iDependency];
        dependencies +='      <dependency name="'+dependency.name+'" version="'+dependency.version+'"/>\n';
      }
      dependencies += '   </dependencies>\n';
    }
    return dependencies;
  };

  var getExportPoints = function(cfg,today){
    var exportpoints = '   <exportpoints/>\n';
    if(cfg.module.exportpoints!==undefined && cfg.module.exportpoints!==false && cfg.module.exportpoints!==null && cfg.module.exportpoints.length>0){
      exportpoints = '   <exportpoints>\n';
      for(var iExportpoint = 0; iExportpoint < cfg.module.exportpoints.length; iExportpoint++){
        var exportpoint = cfg.module.exportpoints[iExportpoint];
        exportpoints +='      <exportpoint uri="'+resolveMacros(exportpoint.src,today,cfg)+'" destination="'+resolveMacros(exportpoint.dest,today,cfg)+'"/>\n';
      }
      exportpoints += '   </exportpoints>\n';
    }
    return exportpoints;
  };

  var getResources = function(cfg,today){
    var resources = '   <resources/>\n';
    if(cfg.module.resources!==undefined && cfg.module.resources!==false && cfg.module.resources!==null && cfg.module.resources.length>0){
      resources = '   <resources>\n';
      for(var iResource = 0; iResource < cfg.module.resources.length; iResource++){
        var resource = cfg.module.resources[iResource];
        resources +='      <resource uri="'+resolveMacros(resource.src,today,cfg)+'"/>\n';
      }
      resources += '   </resources>\n';
    }
    return resources;
  };

  var getParameters = function(cfg,today){
    var parameters = '   <parameters/>\n';
    if(cfg.module.parameters!==undefined && cfg.module.parameters!==false && cfg.module.parameters!==null && cfg.module.parameters.length>0){
      parameters = '   <parameters>\n';
      for(var iParameters = 0; iParameters < cfg.module.parameters.length; iParameters++){
        var parameter = cfg.module.parameters[iParameters];
        parameters +='      <parameter name="'+parameter.name+'" value="'+resolveMacros(parameter.value,today,cfg)+'"/>\n';
      }
      parameters += '   </parameters>\n';
    }
    return parameters;
  };
  
  var getTypesFiles = function(options){
    var typesList = [];
    var allTypeFiles = grunt.file.expand({cwd:options.types,filter: 'isFile'},['**.yml']);
    console.log('Opencms resources types config files : ' + allTypeFiles);
    if(allTypeFiles.length>0){
      for(var i = 0; i < allTypeFiles.length; i++){
        var path = allTypeFiles[i];
        var typefile = grunt.file.readYAML(options.types + path);
        typesList.push(typefile);
      }
    }
    //console.log('Opencms resources types : ');
    //console.log(typesList);
    return typesList;
  };

  var getResourceType = function(type,cfg){
    // si resourcetype: false, on n'ajouter rien
    if(!type.resourcetype){
      return '';
    }

    var properties = '';
    if(type.resourcetype.properties!==undefined && type.resourcetype.properties!==false && type.resourcetype.properties!==null){
      for(var i = 0; i < type.resourcetype.properties.length; i++){
        var prop = type.resourcetype.properties[i];
        properties += '            <property>\n'+
          '               <name>'+prop.name+'</name>\n'+
          '               <value type="'+prop.type+'"><![CDATA['+prop.value+']]></value>\n'+
          '            </property>\n';
      }
    }
    if(properties!==''){
      properties = '         <properties>\n'+properties+'         </properties>\n';
    }else{
      properties = '         <properties/>\n';
    }

    var params = '';
    if(type.resourcetype.params!==undefined && type.resourcetype.params!==false && type.resourcetype.params!==null){
      for(var i = 0; i < type.resourcetype.params.length; i++){
        var param = type.resourcetype.params[i];
        var value = param.value;
        if(param.name==='schema')
          value = '/system/modules/' + cfg.project.package + '/' + value;
        params += '         <param name="'+param.name+'">'+value+'</param>\n';
      }
    }

    var result = '      <type class="'+type.resourcetype.class+'" name="'+type.name+'" id="'+type.resourcetype.id+'">\n'+
      properties+params+'      </type>\n';
    return result;
  };

  var getExplorerType = function(type){
    // si explorertype: false, on n'ajouter rien
    if(!type.explorertype){
      return '';
    }

    var icon = 'xmlcontent.gif';
    if(type.explorertype.icon!==undefined) icon = type.explorertype.icon;
    var bigicon = 'xmlcontent_big.png';
    if(type.explorertype.bigicon!==undefined) bigicon = type.explorertype.bigicon;
    var uri = 'newresource_xmlcontent.jsp?newresourcetype='+type.name;
    if(type.explorertype.uri!==undefined) uri = type.explorertype.uri;
    var order = '1';
    if(type.explorertype.order!==undefined) order = type.explorertype.order;
    var autosetnavigation = 'false';
    if(type.explorertype.autosetnavigation!==undefined) autosetnavigation = type.explorertype.autosetnavigation;
    var autosettitle = 'false';
    if(type.explorertype.autosettitle!==undefined) autosettitle = type.explorertype.autosettitle;
    var reference = 'xmlcontent';
    if(type.explorertype.reference!==undefined) reference = type.explorertype.reference;
    var page = undefined;
    if(type.explorertype.page!==undefined) page = 'page="'+type.explorertype.page+'"';

    var accesscontrols = '';
    if(type.explorertype.accesscontrol!==undefined && type.explorertype.accesscontrol!==false && type.explorertype.accesscontrol!==null){
      for(var i = 0; i < type.explorertype.accesscontrol.length; i++){
        var entry = type.explorertype.accesscontrol[i];
        accesscontrols += '            <accessentry principal="'+entry.principal+'" permissions="'+entry.permissions+'"/>\n';
      }
    }
    if(accesscontrols!==''){
      accesscontrols = '         <accesscontrol>\n'+accesscontrols+'         </accesscontrol>\n';
    }else{
      accesscontrols = '         <accesscontrol/>\n';
    }

    var result = '      <explorertype name="'+type.name+'" key="fileicon.'+type.name+'" icon="'+icon+'" bigicon="'+bigicon+'" reference="'+reference+'">\n'+
      '         <newresource '+page+' uri="'+uri+'" order="'+order+'" autosetnavigation="'+autosetnavigation+'" autosettitle="'+autosettitle+'" info="desc.'+type.name+'"/>\n'+
      accesscontrols+
      '      </explorertype>\n';
    return result;
  };

  var getResourceTypes = function(cfg,file,list){
    try{
      var resourcetypes;
      if(cfg.moduletypesdefinition==='xml'){
        resourcetypes = file;
      }else if(cfg.moduletypesdefinition==='yml'){
        resourcetypes = '';
        var typesList = list;
        if(typesList.length>0){
          for(var i = 0; i < typesList.length; i++){
            var type = typesList[i];
            resourcetypes += getResourceType(type,cfg);
          }
        }
      }
      return resourcetypes;
    }catch(err){
      console.log('getResourceTypes : ' + err);
    }
  };

  var getExplorerTypes = function(cfg,file,list){
    try{
      var explorertypes;
      if(cfg.moduletypesdefinition==='xml'){
        explorertypes = file;
      }else if(cfg.moduletypesdefinition==='yml'){
        explorertypes = '';
        var typesList = list;
        if(typesList.length>0){
          for(var i = 0; i < typesList.length; i++){
            var type = typesList[i];
            explorertypes += getExplorerType(type);
          }
        }
      }
      return explorertypes;
    }catch(err){
      console.log('getExplorerTypes : ' + err);
    }
  };

  var getFiles = function(cfg,dateString){
    var sourceModule = cfg.folders.src.module;
    var allFolders = grunt.file.expand({cwd:sourceModule+'/',filter: 'isDirectory',dot: true},['**','!**/__properties','!**/**.properties']);
    var allFiles = grunt.file.expand({cwd:sourceModule+'/',filter: 'isFile',dot: true},['**','!**/__properties','!**/**.properties']);
    console.log(allFiles);

    function _readProperties(path,isFile){
      // nom du fichier
      var propertiesFile = undefined;
      if(!isFile){
        propertiesFile = path.substr(0,path.lastIndexOf('/')+1) + "__properties/__" + path.substr(path.lastIndexOf('/')+1, path.length) + '.properties';
      }else{
        propertiesFile = path.substr(0,path.lastIndexOf('/')+1) + "__properties/" + path.substr(path.lastIndexOf('/')+1, path.length) + '.properties';
      }
      // existence
      var propertiesFileLoaded;
      if(grunt.file.exists(sourceModule + '/' + propertiesFile)){
        // chargement
        if(cfg.modulepropertiesdefinition.separator==='='){
          try{
            var contents = grunt.file.read(sourceModule + '/' + propertiesFile);
            var lines = contents.split('\n');
            if(lines!==undefined && lines.length>0){
              for(var l = 0 ; l < lines.length; l++){
                var line = lines[l];
                // test c'est bien =
                if(line.indexOf('=')===-1){
                  console.log('No symbol = in file ' + propertiesFile + ' for line ' + line + ', skip it');
                }else{
                  var keypair = line.split('=');
                  if(keypair!==undefined && keypair.length>1){
                    var key = keypair[0];
                    var value = keypair[1];
                    if(propertiesFileLoaded===undefined)
                      propertiesFileLoaded = {};
                    propertiesFileLoaded[key.trim()] = value.trim();
                  }
                }
              }
            }
          }catch(err){
            console.log('No ' + propertiesFile + ':' + err);
          }
        }else if(cfg.modulepropertiesdefinition.separator==='yml'){
          try{
            //check :
            var contents = grunt.file.read(sourceModule + '/' + propertiesFile);
            var lines = contents.split('\n');
            var isOk = true;
            if(lines!==undefined && lines.length>0){
              for(var l = 0 ; l < lines.length; l++){
                var line = lines[l];
                // test c'est bien :
                if(line.trim()!=='' && line.indexOf(': ')===-1){
                  console.log('No symbol ": " in file ' + propertiesFile + ' for line ' + line + ', skip it');
                  isOk = false;
                }
              }
            }
            if(isOk){
              propertiesFileLoaded = grunt.file.readYAML(sourceModule + '/' + propertiesFile);
            }else{
              console.log('Invalid yaml .properties, skip all file.');
            }
          }catch(err){
            //console.log('No ' + propertiesFile);
          }
        }
        console.log(propertiesFileLoaded);
      }
      
      
      return propertiesFileLoaded;
    }

    function _readAccesses(path,isFile){
      // nom du fichier
      var accessesFile = undefined;
      if(!isFile){
        accessesFile = path.substr(0,path.lastIndexOf('/')+1) + "__acl/__" + path.substr(path.lastIndexOf('/')+1, path.length) + '.xml';
      }else{
        accessesFile = path.substr(0,path.lastIndexOf('/')+1) + "__acl/" + path.substr(path.lastIndexOf('/')+1, path.length) + '.xml';
      }
      var accessesFileLoaded;
      if(grunt.file.exists(sourceModule + '/' + accessesFile)){
        try{
          accessesFileLoaded = grunt.file.read(sourceModule + '/' + accessesFile);
        }catch(err){
          //console.log('No ' + accessesFile);
        }
      }
      console.log(accessesFileLoaded);
      return accessesFileLoaded;
    }

    function _getType(isFile,propertiesFileLoaded){
      var type = '0';
      if(!isFile)
        type = 'folder';
      if(isFile)
        type = 'plain';
      if(propertiesFileLoaded!==undefined && propertiesFileLoaded[cfg.modulepropertiesdefinition.resourcetype]!==undefined){
        type = propertiesFileLoaded.resourcetype;
      }
      return type;
    }

    function _getResourceUUID(propertiesFileLoaded){
      var uuid;
      if(propertiesFileLoaded!==undefined && propertiesFileLoaded[cfg.modulepropertiesdefinition.resourceUUID]!==undefined){
        uuid = propertiesFileLoaded.resourceUUID;
      }
      return uuid;
    }

    function _getStructureUUID(propertiesFileLoaded){
      var uuid;
      if(propertiesFileLoaded!==undefined && propertiesFileLoaded[cfg.modulepropertiesdefinition.structureUUID]!==undefined){
        uuid = propertiesFileLoaded.structureUUID;
      }
      return uuid;
    }

    function addFiles(list,isFile){
      try{
        var files = '';
        if(list.length>0){
          for(var i = 0; i < list.length; i++){
            var path = list[i];
            //path = path.replace(sourceModule, '');
            if(path != ''){
              console.log('File : ' + path);
              var propertiesFileLoaded = _readProperties(path,isFile);
              var accessesFileLoaded = _readAccesses(path,isFile);
              var type = _getType(isFile,propertiesFileLoaded);
              var resourceuuid = _getResourceUUID(propertiesFileLoaded);
              var structureuuid = _getStructureUUID(propertiesFileLoaded);

              var properties = '';
              if(propertiesFileLoaded!==undefined){
                for(var key in propertiesFileLoaded){
                  // TODO : check if key is not a integer (if yaml config but = found, all letters as value)
                  if(key!==undefined && key!==null){
                    if(key!=='resourcetype' && key!=='resourceUUID'){
                      var propertytype = key.substr(key.length-2,key.length)==='.s' ? 'type="shared"' : '';
                      console.log('Prop ' + key + ' = ' + propertiesFileLoaded[key] + ' type:' + propertytype);
                      properties += '            <property '+propertytype+' name="'+key.substring(0,key.length-2)+'" value="<![CDATA['+propertiesFileLoaded[key]+']]>" />\n';
                    }
                  }else{
                    //console.log('Prop found without key, please check yaml format. Skip line.');
                  }
                }
              }
              var propertiesAll = '         <properties/>\n';
              if(properties!==''){
                propertiesAll = '         <properties>\n';
                propertiesAll += properties;
                propertiesAll += '         </properties>\n';
              }

              var accesses = '';
              if(accessesFileLoaded!==undefined){
                
              }
              var accessesAll = '         <accesscontrol/>\n';
              if(accesses!==''){
                accessesAll = '         <accesscontrol>\n';
                accessesAll += accesses;
                accessesAll += '         </accesscontrol>\n';
              }

              files +='      <file>\n';
              if(isFile)
                files +='         <source>'+path+'</source>\n';
              files +='         <destination>'+path+'</destination>\n';
              files +='         <type>'+type+'</type>\n';
              if(isFile)
                files +='         <uuidresource>'+emptyIfNull(resourceuuid)+'</uuidresource>\n';
              files +='         <uuidstructure>'+emptyIfNull(structureuuid)+'</uuidstructure>\n';
              files +='         <datelastmodified>'+dateString+'</datelastmodified>\n';
              files +='         <userlastmodified>Admin</userlastmodified>\n';
              files +='         <datecreated>'+dateString+'</datecreated>\n';
              files +='         <usercreated>Admin</usercreated>\n';
              files +='         <flags>0</flags>\n';
              files += propertiesAll;
              files += accessesAll;
              files +='      </file>\n';

              //console.log(' ');
            }
          }
        }
        return files;
      }catch(err){
        console.log('addfiles : ' + err);
      }
      
    }

    var files = '   <files/>\n';
    if(allFolders.length>0 || allFiles.length>0){
      files = '   <files>\n';
    }
    files += addFiles(allFolders,false);
    files += addFiles(allFiles,true);
    if(allFolders.length>0 || allFiles.length>0){
      files += '   </files>\n';
    }
    return files;
  }; 

  var emptyIfNull = function(value){
    if(value===undefined || value===null){
      return '';
    }
    return value;
  };
  
  var resolveMacros = function(value,today,cfg){
    if(value===undefined || value===null){
      return '';
    }
    var dateString = grunt.template.date(today, "dd mmm yyyy HH:mm:ss");
    //TODO : regex propre generique
    value = value.replace('%(module.name)',cfg.module.name);
    value = value.replace('%(module.version)',cfg.module.version);
    value = value.replace('%(date)',dateString);
    return value;
  };

  grunt.registerMultiTask('manifest', 'Create OpenCms module manifest', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
     project: 'etc/config/project.yml',
     moduleexport: 'etc/config/moduleexport.yml',
     types: 'etc/config/types/'

    });
    //grunt.log.writeln('Final options = ' + JSON.stringify(options, null, 4));
    //grunt.log.writeln('arguments = ' + JSON.stringify(arguments, null, 4));

    var project = grunt.file.readYAML(options.project);
    var moduleexport = grunt.file.readYAML(options.moduleexport);
    var resourcetypes = grunt.file.read(options.types + 'resourcetypes.xml');
    var explorertypes = grunt.file.read(options.types + 'explorertypes.xml');
    var typesList = getTypesFiles(options);

    //grunt.log.writeln(JSON.stringify(moduleexport.info, null, 4));
    //grunt.log.writeln(JSON.stringify(moduleexport.module, null, 4));

    var today = new Date();
    var dateString = grunt.template.date(today, "ddd, dd mmm yyyy HH:mm:ss") + ' CEST';

       
    var contents = '<?xml version="1.0" encoding="UTF-8"?>\n';
    contents += 
      '<export>\n'+
      ' <info>\n'+
      '   <creator>'+moduleexport.info.creator+'</creator>\n'+
      '   <opencms_version>'+moduleexport.info.opencmsversion+'</opencms_version>\n'+
      '   <createdate>'+dateString+'</createdate>\n'+
      '   <infoproject>'+moduleexport.info.infoproject+'</infoproject>\n'+
      '   <export_version>'+moduleexport.info.exportversion+'</export_version>\n'+
      ' </info>\n'+
      ' <module>\n'+
      '   <name>'+moduleexport.module.name+'</name>\n'+
      '   <nicename>'+resolveMacros(moduleexport.module.nicename,today,moduleexport)+'</nicename>\n'+
      '   <group>'+moduleexport.module.group+'</group>\n'+
      '   <class>'+emptyIfNull(moduleexport.module.class)+'</class>\n'+
      '   <description>'+resolveMacros(moduleexport.module.description,today,moduleexport)+'</description>\n'+
      '   <version>'+moduleexport.module.version+'</version>\n'+
      '   <authorname><![CDATA['+moduleexport.module.authorname+']]></authorname>\n'+
      '   <authoremail><![CDATA['+moduleexport.module.authoremail+']]></authoremail>\n'+
      '   <datecreated>'+dateString+'</datecreated>\n'+
      '   <userinstalled>'+emptyIfNull(moduleexport.module.userinstalled)+'</userinstalled>\n'+
      '   <dateinstalled>'+emptyIfNull(moduleexport.module.dateinstalled)+'</dateinstalled>\n'+
      getDependencies(moduleexport)+
      getExportPoints(moduleexport,today)+
      getResources(moduleexport,today)+
      '   <resourcetypes>\n'+
      emptyIfNull(getResourceTypes(project,resourcetypes,typesList))+
      '   </resourcetypes>\n'+
      '   <explorertypes>\n'+
      emptyIfNull(getExplorerTypes(project,explorertypes,typesList))+
      '   </explorertypes>\n'+
      getParameters(moduleexport,today)+
      ' </module>\n'+
      getFiles(project,dateString)+
      '</export>\n';

    grunt.file.write( project.folders.dist.module+'/manifest.xml', contents);
    
  });

}