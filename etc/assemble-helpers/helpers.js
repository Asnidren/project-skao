var merge = require('mixin-deep');
module.exports.register = function (Handlebars, options, params)  { 
	Handlebars.registerHelper('renderpartial', function(name, ctx, hash) {
		var fn;

		//console.log('Template...');
		//console.log(name);
		var template = Handlebars.partials[name];
		//console.log(template);
		if (typeof template !== 'Function' ){
			try{
				fn = Handlebars.compile(template);
			}catch(err){
				console.error('Cannot compile '+name+' :'+err);
			}
		}else{
			fn = template;
		}
		if(fn !== undefined){
			var output = fn(merge(this, ctx)).replace(/^\s+/, '');
			//console.log('Output...');
			//console.log(output);
			return new Handlebars.SafeString(output);
		}

		return new Handlebars.SafeString('');
	});
	Handlebars.registerHelper('safeVal', function(value, safeValue){
		var out = value || safeValue;
		return new Handlebars.SafeString(out);
	});
};