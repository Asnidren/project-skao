# project-skao

> This repo contains files needed to start quickly an OpenCms website module project. It can be used too tp build statics projects only.

## Installation

Project build is based on Grunt tasks.

1. Download files in your project folder.

2. Install all needed Grunt tasks with :

```shell
npm install
```

## Configuration

Copy *sample/config/* folder into *etc/* folder, and customise your project configuration (See below for options):

* *etc/config/project.yml* : project infos, configuration modes, and project folders, 
* *etc/config/moduleexport.yml* : OpenCms module infos,
* *etc/config/moduleconfig.yml* : OpenCms module .config options,
* *etc/config/types/xxx.yml* : Static components / OpenCms modules types

Then, initialize project subfolders :

```shell
grunt init           // create sources and build folders, initialize types files and module .config file
```

Now, you can start your project!


## Getting Started

### Static templates and API pages

####Add assets :

* your sass files in "src/assets/scss", 
* your javascript files in "src/assets/js",
* your images files in "src/assets/img",

You can run :
```shell
grunt css		// to compile scss and update styles.min.css in "build/css"
```
```shell
grunt js		// to jshint, uglify and update scripts.min.js in "build/js"
```

####Add html templates :

* your page templates in "src/html/hbs/templates",
* your formatters templates in "src/html/hbs/formatters" (should be created automatically by task "grunt init" if pre-configured by types in etc/config/types/...),
* your other partials templates in "src/html/hbs/nested"

Add html contents (managed with YAML front-ends) :

* your static pages with YAML front-ends into "src/html/bodies/static",
* your api pages with YAML front-ends into "src/html/bodies/api", 
* add other yml contents in "src/html/data/..."

Compile all that with :
```shell
grunt html 		// to compile .hbs for static .html / api .html / formatters .jsp in "build/dist-static", "build/dist-api", and "build/dist-module/.../formatters"
```


####And full statics :

```shell
grunt static 	// compile sass, run jshint, uglify, and compile hbs
```

Test your result in "build/dist-static/" and "build/dist-api/".


### OpenCms module

#### New method configuration

The new method use power of YML config to generate automatically several OpenCms files.

* In *etc/config/project.yml*, set variables :
```yml
moduletypesdefinition: yml
modulepropertuesdefinition:
  separator: yml
  resourcetype: resourcetype
  resourceuuid: resourceuuid
  structureuuid: structureuuid
```

* Add OpenCms types as YML files in *etc/config/types/*.
* Configure *etc/config/moduleconfig.yml*.
* Run "grunt init".

Schemas XSD files and formatters are automatically initialized, you just have to fill it. Types are automatically added to the manifest and ".config" files.
Module ".config" is automatically created.



#### Old method configuration

* In *etc/config/project.yml*, set variables :
```yml
moduletypesdefinition: xml
modulepropertuesdefinition:
  separator: =
  resourcetype: EurelisProperty.type
  resourceuuid: EurelisProperty.resourceUUID
  structureuuid: EurelisProperty.structureUUID
```

* Add OpenCms types as XML contents in files *etc/config/types/resourcetypes.xml* and *etc/config/types/explorertypes.xml*.
* You have to create all files of the module.



### Construct your module

* Complete your OpenCms module in "src/module".
* Add java source to be complied in "src/java", will be automatically generated as .jar file and added to the final module zip.
* external libs needed to compile java in "etc/complib"
* external libs needed to compile java and to be imported on OpenCms with the module must be in "etc/distlib",
* You can use .hbs to compile basic formatters as the same time as static page, in order to maintain static templates and api during corrections & evolutions, but be carefull to not overwrite files
* css compiled file, js minified file, and optimized images will be automatically added during module distribution

```shell
grunt java 		// to generate MANIFEST.MF, compile java classes, build jar, and generate javadoc
```
```shell
grunt inittypes    // to initialize module types files (XSD, formatters) - only works with YAML config - (this task do not erase files if they already exists, just create new ones)
```
```shell
grunt updatemoduleconfig    // to generate the module .config file - only works with YAML config - (update all the file, all config must be set in configuration files)
```
```shell
grunt module 	// to distribute module files, create manifest, and compress to zip
```

#### Add properties to module resources :

* add some "__properties" folder in the same level of the resource
* add inside this folder some "my-filename-with-extension.properties" file (or "__my-foldername.properties" for folders)
* write properties in this file as YAML content 
```yml
Title.i: My title
Description.s: My shared description
```
Special key/pair values for resource type and uuid are :
```yml
resourcetype: imagegallery
resourceUUID: 00:C0:F0:3D:5B:7C
structureUUID: 00:C0:F0:3D:5B:7C
```

#### Add accesses to module resources :

* add some "__acl" folder in the same level of the resource
* add inside this folder some "my-filename-with-extension.xml" file
* add inside this folder some "my-filename-with-extension.properties" file (or "__my-foldername.properties" for folders)
* write accesses in this file as xml content 


## Config options


**project.yml**

* *project.package* is the OpenCms module folder name. It's used as a folder name under "/system/module/", as a lib package, and as a folder tree for bundles.
* *java.jdk* is used by javac command to compile java clases.
* *moduletypesdefinition* is the configuration mode for getting resourcetypes and explorertypes. Available values are : 

```
#!javascript

* xml : read file contents of "etc/config/types/resourcetypes.xml" and "etc/config/types/explorertypes.xml" and write it into module manifest directly
* yml : read file contents of "*.yml" files into "etc/config/types/", parse it as YAML contents, and use values to generate manifest contents of resourcetypes and explorertypes. It's also used by tasks to initialize or check related files (xsd, jsp...).
```


* *modulepropertiesdefinition.separator* is the configuration mode for getting type, uuid, and properties of module resources, in files named "**.properties" (see above). Available values are :

```
#!javascript

* = : mode used by Eurelis ant task.
* yml : key/pairs values must be formatted as YAML text.
```


**moduleexport.yml**

This file contains all parameters set in the module manifest. You should check all. Only dates are automatic, and set as current build date.


## NotaBene / Remains to do

This Grunt config has been created and tested on Windows7.

This Grunt config is inspired from the Eurelis OpenCms Ant task for Eclipse.

## Help